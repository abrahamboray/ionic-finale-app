import { Component,ViewChild } from '@angular/core';
 import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Nav, Platform,ToastController } from 'ionic-angular';

import { HomePage } from '../pages/home/home';
import { OnlineTodosPage } from '../pages/online-todos/online-todos';
import { TodosArchivePage } from '../pages/todos-archive/todos-archive';
import { Firebase } from '@ionic-native/firebase';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;

  rootPage:any = HomePage;
  pages: Array<{title: string, component: any}>;

  constructor(public  platform: Platform, public  statusBar: StatusBar, public  splashScreen: SplashScreen,private firebase: Firebase,public toastCtrl: ToastController
 ) {

  

    platform.ready().then(() => {


       try{
        this.initializeFirebase();
      } catch (error) {
        this.firebase.logError(error);
      }

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    }); this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Local TODOS', component: TodosArchivePage },
      { title: 'Online TODOS', component: OnlineTodosPage }
    ];

  }


initializeFirebase() {
    if(!this.platform.is("core")) {
      this.firebase.subscribe("all");
      this.platform.is('android') ? this.initializeFirebaseAndroid() : this.initializeFirebaseIOS();
    }
  }
initializeFirebaseAndroid() {
    this.firebase.getToken().then(token => {});
    this.firebase.onTokenRefresh().subscribe(token => {})
    this.subscribeToPushNotifications();
  }
initializeFirebaseIOS() {
    this.firebase.grantPermission()
    .then(() => {
      this.firebase.getToken().then(token => {});
      this.firebase.onTokenRefresh().subscribe(token => {})
      this.subscribeToPushNotifications();
    })
    .catch((error) => {
      this.firebase.logError(error);
    });
  }
subscribeToPushNotifications() {
    this.firebase.onNotificationOpen().subscribe((response) => {
      if(response.tap){
        //Received while app in background (this should be the callback when a system notification is tapped)
        //This is empty for our app since we just needed the notification to open the app
      }else{
        //received while app in foreground (show a toast)
        let toast = this.toastCtrl.create({
          message: response.body,
          duration: 3000
        });
        toast.present();
      }
    });
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
   // used for an example of ngFor and navigation
   
}


import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
 import { Media, MediaObject } from '@ionic-native/media';
import { HttpModule } from '@angular/http';
import { Firebase } from '@ionic-native/firebase';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {TodosArchivePage} from '../pages/todos-archive/todos-archive';
import { OnlineTodosPage } from '../pages/online-todos/online-todos';
import { TodoProvider } from '../providers/todo/todo';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TodosArchivePage,

    OnlineTodosPage
  ],
  imports: [HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ], 
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TodosArchivePage,
    OnlineTodosPage 

  ],
  providers: [ 
    StatusBar, 
    SplashScreen,Firebase,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    TodoProvider,Media  
  ]
})
export class AppModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController,LoadingController } from 'ionic-angular';
import { Http} from '@angular/http';

/**
 * Generated class for the OnlineTodosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 
@Component({
  selector: 'page-online-todos',
  templateUrl: 'online-todos.html',
})
export class OnlineTodosPage {
	public onlineTodos = [];
	public page = 1;
	public loadinC= this.loadingCtrl.create({


		    		content: "Loading... please wait!"

		    	});
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public toastCtrl:ToastController,public loadingCtrl:LoadingController) {
		    	this.loadinC.present();

		 
  	   let url = "http://localhost/ionic/wp-json/wp/v2/posts?page="+this.page+"&per_page=1&order_by=date&order=asc";

		    this.http.get(url).map(res => res.json()).subscribe(data => {

 

		        this.onlineTodos.push(data[0]);
		        console.log('IN GET URL!')
		        
				this.page++;
		    },err=>{

		let toast= this.toastCtrl.create({message:"That's all buddy !",duration:3000,position:"bottom"});
		toast.present();

  		    },()=>{this.loadinC.dismiss();		        console.log('DONE LOADING!')
});
			




  }


  	  doRefresh(event){
  	  	  let url = "http://localhost/ionic/wp-json/wp/v2/posts?page="+this.page+"&per_page=1&order_by=date&order=asc";

		    this.http.get(url).map(res => res.json()).subscribe(data => {

		        this.onlineTodos.push(data[0]);
		        console.log(data);
		        event.complete();
				this.page++;
		    },err=>{

		let toast= this.toastCtrl.create({message:"That's all buddy !",duration:3000,position:"bottom"});
		toast.present();

console.log('OOPS');
 event.complete();
		    });





console.log('REFRESHED ...!')}











 





  ionViewDidLoad() {
    console.log('ionViewDidLoad OnlineTodosPage');
  }

}

import { Component } from '@angular/core';
import { NavController, AlertController,reorderArray } from 'ionic-angular';
import { TodosArchivePage } from '../todos-archive/todos-archive';
import { OnlineTodosPage } from '../online-todos/online-todos';
import { TodoProvider } from '../../providers/todo/todo';
import { Firebase } from '@ionic-native/firebase';

 
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	public  todos = []; 
 
	public isReorder =false;
  public archivePage = TodosArchivePage;  
  public thisonlineTodosPage = OnlineTodosPage; 

  constructor(public navCtrl: NavController,public alert:AlertController,public todoservice:TodoProvider,private firebase: Firebase) {
  	this.todos =    this.todoservice.getTodos();
 /*this.firebase.getToken()
  .then(token => console.log('The token is ${token}')) // save the token server-side and use it to push notifications to this device
  .catch(error => console.error('Error getting token', error));

this.firebase.onTokenRefresh()
  .subscribe((token: string) => console.log('Got a new token ${token}')); */

  }    

 



    reorrderEnabled(){ this.isReorder=!this.isReorder; }

   reorrderDisabled(){  this.isReorder=!this.isReorder;    }


   valideteReorderPlease(indexes){     reorderArray(this.todos,indexes);   }


archiveThis(i){this.todoservice.setArchivedTodo(i);}


goToArchive(){ this.navCtrl.push(this.archivePage);}



goToOnlineArchive(){ this.navCtrl.push(this.thisonlineTodosPage);}


  openModal(){
  	let alert = this.alert.create({
  			title:"Add new todo !",
  			buttons:[{text:"Add",handler:data=>{
			this.todoservice.setTodo(data.todoInput)
 			}}],
  			inputs: [{
  				name :'todoInput',
  				placeholder :'Type new todo text ..'


  			}]


  		});

  	alert.present();

  }

}

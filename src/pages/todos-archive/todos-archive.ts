import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TodoProvider } from '../../providers/todo/todo';
/**
 * Generated class for the TodosArchivePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 

@Component({
  selector: 'page-todos-archive',
  templateUrl: 'todos-archive.html',
})
export class TodosArchivePage {
 	  public archivedTodos = [];
  constructor(public navCtrl: NavController, public navParams: NavParams,public todoservice : TodoProvider) {
  	 this.archivedTodos=this.todoservice.getArchivedTodos();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TodosArchivePage');
  }


 deleteThis(i){this.todoservice.deletThis(i);}
}
